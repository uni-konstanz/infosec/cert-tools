#!/bin/sh
###
# Copyright (c) 2016 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 
###
# Extract SSH private and public keys from S/MIME PKCS12 certificate.
###

# in POSIX undefined
#set -eu -o pipefail
#shopt -s failglob

usage() {
    echo "usage: $(basename "$0") PKCS12_CERTIFICATE"
	echo ""
	echo "Extract SSH private keys from PKCS12 certificate, generate public key, and write them to ~/.ssh/"
	exit 0;
}

# check parameters
CERT="$1"
[ -z "$CERT" ] && usage;
[ ! -f "$CERT" ] && echo "ERROR: file ${CERT} not found!" && usage;

# set variables
base="$(basename "${CERT}")";
#ext="${base##*.}";
idcert="id_${base%.*}";

# check for ~/.ssh
if [ ! -d ~/.ssh ]; then
	mkdir -p ~/.ssh;
	chmod 700 ~/.ssh;
fi

# Extract and encrypt private key
# Must be done in a single step to reduce risk of leaking the key
exprvkey() {
	echo "Extracting and encrypting private key."
	#openssl pkcs12 -in "${CERT}" -nodes -nocerts | (sleep 5; openssl rsa -aes256 -in /proc/self/fd/0 -out ~/.ssh/${idcert} && chmod 600 ~/.ssh/${idcert});
	openssl pkcs12 -in "${CERT}" -nodes -nocerts | (sleep 5; openssl rsa -aes256 -in /proc/self/fd/0 -out ~/.ssh/"${idcert}" && chmod 600 ~/.ssh/"${idcert}");
}

# Extract public key
expubkey() {
	echo "Generate public key."
	ssh-keygen -y -f ~/.ssh/"${idcert}" > ~/.ssh/"${idcert}".pub;
	chmod 640 ~/.ssh/"${idcert}".pub
}

main() {
	exprvkey;
	expubkey;
}

main;
