# cert-tools

	Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
    	Usage of the works is permitted provided that this instrument is retained with the works, so that     any entity that uses the works is notified of this instrument.
    	DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

Useful tools to process and convert certificates

- cert2ssh  
  Extract SSH private and public keys from S/MIME PKCS12 certificate.

- cert-convert  
  Convert certificates to and from different formats

- remove-cert-pw  
  remove passwort from smime certificate


