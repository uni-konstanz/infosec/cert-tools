#!/bin/sh
###
# Copyright (c) 2016 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 
###
# Convert certificates to and from different formats
###

set -eu
# in POSIX undefined
#set -eu -o pipefail
#shopt -s failglob

usage () {
    echo "usage: $(basename "$0") CERTIFICATE [PRIVATE_KEY]"
	echo ""
	echo "Converts certificates from and to different formats."
	echo "If a PEM cert and private key are provided, they are converted to a pkcs12 cert."
	exit 0;
}

pemkey_2_pkcs12 () {
	echo "convert cert $1 and key $2 to cert.p12"
	openssl pkcs12 -export -inkey "$2" -in "$1" -out cert.p12 # -certfile CAcert.crt
}

pkcs12_2_pem () {
	echo "extract private key from $1 to ${fname}_privkey.pem"
	openssl pkcs12 -in "$1" -nocerts -out "${fname}"_privkey.pem

	echo "extract clcerts from $1 to ${fname}_clcerts.pem"
	openssl pkcs12 -in "$1" -clcerts -nokeys -out "${fname}"_clcerts.pem

	echo "convert $1 to ${fname}.pem"
	openssl pkcs12 -in "$1" -out "${fname}".pem #-nodes;
}

pem_2_der () {
	echo "convert $1 to ${fname}.der"
	openssl x509 -in "$1" -outform der -out "${fname}".der
}

der_2_pem () {
	echo "convert $1 to ${fname}.pem"
	openssl x509 -inform der -in "$1" -out "${fname}".pem
}

convert () {
	if [ "$1" = "" ]; then echo "ERROR: require certificate" && return; fi
	base=$(basename "$1")
	ext="${base##*.}"
	fname="${base%.*}"

	if [ "$ext" = "p12" ] || [ "$ext" = "pfx" ]; then
		pkcs12_2_pem "$1" "$fname";
	elif [ "$ext" = "pem" ]; then
		pem_2_der "$1" "$fname";
	elif [ "$ext" = "der" ] || [ "$ext" = "cer" ] || [ "$ext" = "crt" ]; then
		der_2_pem "$1" "$fname";
	fi
}

if [ "$#" -lt 1 ]; then echo "ERROR: require at least a certificate" && usage; fi
if [ "$#" = 2 ]; then
	pemkey_2_pkcs12 "$1" "$2";
	exit;
fi

convert "$1";
